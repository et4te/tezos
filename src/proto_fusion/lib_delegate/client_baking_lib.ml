(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Proto_fusion
open Fusion_context

let bake_block
    (cctxt : #Proto_fusion.full)
    ?(chain = `Main)
    ?minimal_fees
    ?minimal_nanotez_per_gas_unit
    ?minimal_nanotez_per_byte
    ?(await_endorsements = false)
    ?force
    ?max_priority
    ?(minimal_timestamp = false)
    ?mempool
    ?context_path
    ?src_sk
    ?src_pk
    block
    delegate =
  begin
    match src_sk with
    | None ->
        Client_keys.get_key cctxt delegate >>=? fun (_, _, src_sk) ->
        return src_sk
    | Some sk -> return sk
  end >>=? fun src_sk ->
  begin
    match src_pk with
    | None ->
        Client_keys.get_key cctxt delegate >>=? fun (_, src_pk, _) ->
        return src_pk
    | Some pk -> return pk
  end >>=? fun src_pk ->
  Fusion_services.Helpers.current_level
    cctxt ~offset:1l (chain, block) >>=? fun level ->
  let seed_nonce, seed_nonce_hash =
    if level.expected_commitment then
      let seed_nonce = Client_baking_forge.generate_seed_nonce () in
      let seed_nonce_hash = Nonce.hash seed_nonce in
      Some seed_nonce, Some seed_nonce_hash
    else
      None, None in
  Client_baking_forge.forge_block cctxt
    ?force
    ?minimal_fees
    ?minimal_nanotez_per_gas_unit
    ?minimal_nanotez_per_byte
    ~await_endorsements
    ?timestamp:(if minimal_timestamp then None else Some (Time.now ()))
    ?seed_nonce_hash
    ?mempool
    ?context_path
    ~priority:(`Auto (delegate, max_priority))
    ~src_sk
    block >>=? fun block_hash ->
  let src_pkh = Signature.Public_key.hash src_pk in
  Client_baking_forge.State.record cctxt src_pkh level.level >>=? fun () ->
  begin match seed_nonce with
    | None -> return_unit
    | Some seed_nonce ->
        Client_baking_nonces.add cctxt block_hash seed_nonce
        |> trace_exn (Failure "Error while recording block")
  end >>=? fun () ->
  cctxt#message "Injected block %a" Block_hash.pp_short block_hash >>= fun () ->
  return_unit

let endorse_block cctxt delegate =
  Client_keys.get_key cctxt delegate >>=? fun (_src_name, src_pk, src_sk) ->
  Client_baking_endorsement.forge_endorsement cctxt
    cctxt#block ~src_sk src_pk >>=? fun oph ->
  cctxt#answer "Operation successfully injected in the node." >>= fun () ->
  cctxt#answer "Operation hash is '%a'." Operation_hash.pp oph >>= fun () ->
  return_unit

let get_predecessor_cycle (cctxt : #Client_context.printer) cycle =
  match Cycle.pred cycle with
  | None ->
      if Cycle.(cycle = root) then
        cctxt#error "No predecessor for the first cycle"
      else
        cctxt#error
          "Cannot compute the predecessor of cycle %a"
          Cycle.pp cycle
  | Some cycle -> Lwt.return cycle

let do_reveal cctxt block blocks =
  let nonces = List.map snd blocks in
  Client_baking_revelation.forge_seed_nonce_revelation
    cctxt block nonces >>=? fun () ->
  return_unit

let reveal_block_nonces (cctxt : #Proto_fusion.full) block_hashes =
  cctxt#with_lock begin fun () ->
    Client_baking_nonces.load cctxt
  end >>=? fun nonces ->
  Lwt_list.filter_map_p
    (fun hash ->
       Lwt.catch
         (fun () ->
            Client_baking_blocks.info cctxt (`Hash (hash, 0)) >>= function
            | Ok bi -> Lwt.return_some bi
            | Error _ ->
                Lwt.fail Not_found)
         (fun _ ->
            cctxt#warning
              "Cannot find block %a in the chain. (ignoring)@."
              Block_hash.pp_short hash >>= fun () ->
            Lwt.return_none))
    block_hashes >>= fun block_infos ->
  filter_map_s (fun (bi : Client_baking_blocks.block_info) ->
      match Block_hash.Map.find_opt bi.hash nonces with
      | None ->
          cctxt#warning "Cannot find nonces for block %a (ignoring)@."
            Block_hash.pp_short bi.hash >>= fun () ->
          return_none
      | Some nonce ->
          return_some (bi.hash, (bi.level, nonce)))
    block_infos >>=? fun blocks ->
  do_reveal cctxt cctxt#block blocks

let reveal_nonces cctxt () =
  Client_baking_forge.get_unrevealed_nonces
    cctxt cctxt#block >>=? fun nonces ->
  do_reveal cctxt cctxt#block nonces >>=? fun () ->
  Client_baking_forge.filter_outdated_nonces cctxt cctxt#block >>=? fun () ->
  return_unit

(* Voting functionality *)

let get_voting_period (cctxt : #Proto_fusion.full) =
  Fusion_block_services.metadata cctxt ~chain:`Main ~block:cctxt#block () >>=?
    fun { protocol_data = { voting_period_kind } } ->
    (match voting_period_kind with
     | Voting_period.Proposal ->
        cctxt#message "Voting period is 'Proposal'." >>= fun () ->
        return_unit
     | Voting_period.Testing_vote ->
        cctxt#message "Voting period is 'Testing_vote'." >>= fun () ->
        return_unit
     | Voting_period.Testing ->
        cctxt#message "Voting period is 'Testing'." >>= fun () ->
        return_unit
     | Voting_period.Promotion_vote ->
        cctxt#message "Voting period is 'Promotion_vote'." >>= fun () ->
        return_unit
    ) >>=? fun () ->
    return_unit

let sign cctxt ?watermark src_sk shell (Contents_list contents) =
  let bytes =
    Data_encoding.Binary.to_bytes_exn
      Operation.unsigned_encoding
      (shell, (Contents_list contents)) in
  Client_keys.sign cctxt ?watermark src_sk bytes >>=? fun signature ->
  let protocol_data = Operation_data { contents ; signature = Some signature } in
  return { shell ; protocol_data }

let bake (cctxt : #Proto_fusion.full) delegate operations =
  Client_keys.get_key cctxt delegate >>=? fun (_, src_pk, src_sk) ->
  let src_pkh = Signature.Public_key.hash src_pk in
  Fusion_services.Helpers.current_level
    cctxt ~offset:1l (`Main, cctxt#block) >>=? fun level ->
  let seed_nonce_hash =
    if level.Level.expected_commitment then
      let seed_nonce =
        match Nonce.of_bytes @@
                Rand.generate Constants.nonce_length with
        | Error _ ->
           assert false
        | Ok nonce ->
           nonce
      in
      Some (Nonce.hash seed_nonce)
    else
      None
  in
  Client_baking_forge.forge_block
    cctxt
    ~operations
    ~force:true
    ~best_effort:false
    ~sort:false
    ~priority:(`Auto (src_pkh, Some 1024))
    ?seed_nonce_hash
    ~src_sk
    cctxt#block

let propose (cctxt : #Proto_fusion.full) delegate proposals =
  Client_keys.get_key cctxt delegate >>=? fun (_, src_pk, src_sk) ->
  let src_pkh = Signature.Public_key.hash src_pk in
  Shell_services.Blocks.hash cctxt ~block:cctxt#block () >>=? fun hash ->
  Fusion_services.Helpers.current_level
    cctxt ~offset:1l (`Main, cctxt#block) >>=? fun level ->
  let shell = { Tezos_base.Operation.branch = hash } in
  let contents =
    Proposals { source = src_pkh ;
                period = level.voting_period ;
                proposals }
  in
  sign cctxt ~watermark:Generic_operation src_sk shell (Contents_list (Single contents))
  >>=? fun op ->
  bake cctxt delegate [op]
  >>=? fun _head ->

  (* Injection.inject_operation cctxt ~chain:`Main ~block:cctxt#block
   *   ~fee_parameter:Injection.dummy_fee_parameter ~confirmations:0
   *   ~src_sk (Single contents) >>=? fun _res -> *)

  (* cctxt#message "Proposal submitted: %a\n"
   *   Data_encoding.Json.pp
   *     (Data_encoding.Json.construct Operation.encoding res) >>= fun () -> *)

  return_unit

exception Invalid_ballot

let to_ballot s =
  match String.lowercase_ascii s with
  | "yay" | "yea" ->
     Vote.Yay
  | "nay" ->
     Vote.Nay
  | "pass" ->
     Vote.Pass
  | _ ->
     raise Invalid_ballot
  (* | s ->
   *    failwith "Invalid ballot: '%s'" s *)

let vote (cctxt : #Proto_fusion.full) source proposal ballot_string =
  let ballot = to_ballot ballot_string in
  Client_proto_context.get_manager
    cctxt ~chain:`Main ~block:cctxt#block source
  >>=? fun (_src_name, src_pkh, _src_pk, src_sk) ->
  Fusion_services.Helpers.current_level cctxt ~offset:1l (`Main, cctxt#block) >>=? fun (level : Level.t) ->
  let period = level.voting_period in
  let contents = Single ( Ballot { source = src_pkh ; period ; proposal ; ballot } ) in
  Injection.inject_operation cctxt ~chain:`Main ~block:cctxt#block ~confirmations:0
    ~fee_parameter:Injection.dummy_fee_parameter ~src_sk contents >>=? fun _res ->
  return_unit
