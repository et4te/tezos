open Proto_fusion
open Fusion_context

include Tezos_stdlib.Logging.Make_semantic(struct let name = "client.prevote" end)

(* open Logging *)

module State = Daemon_state.Make(struct let name = "prevote" end)

(** Wrap the state in a type to increase code readability *)

type 'a t = {
    cctxt : #Proto_fusion.full as 'a;
    chain : Chain_services.chain;
    block : Block_services.block;
    height : Raw_height.t;
  }

let create cctxt block height = {
    cctxt; chain = `Main; block; height
  }

(* Fusion_services *)

let get_endorsing_rights t delegate = 
  Fusion_services.Delegate.Endorsing_rights.get t.cctxt
    ~levels:[Raw_level.of_int32_exn (Raw_height.to_int32 t.height)]
    ~delegates:[delegate]
    (t.chain, t.block)

(** Creates a new prevote operation and serialises it to bytes *)

let forge_prevote t hash round block_hash =
  Fusion_services.Forge.prevote t.cctxt
    ~branch:hash
    ~height:t.height
    ~round
    ~block_hash
    (t.chain, t.block)

(* Shell_services *)

let get_chain_id t =
  Shell_services.Chain.chain_id t.cctxt ~chain:t.chain ()

let hash_block t =
  Shell_services.Blocks.hash t.cctxt ~chain:t.chain ~block:t.block ()

let inject_operation t ?async signed_bytes =
  Shell_services.Injection.operation t.cctxt ?async ~chain:t.chain signed_bytes

(* Fusion_block_services *)

let get_block_metadata t =
  Fusion_block_services.metadata t.cctxt ~chain:t.chain ~block:t.block ()

(* Signature *)

let public_key_hash src_pk =
  Signature.Public_key.hash src_pk
    
(* Client_keys *)

let get_client_key t src_pkh =
  Client_keys.get_key t.cctxt src_pkh

(** Produces a buffer of the form <bytes:signature> *)

let sign_prevote t src_sk bytes =
  Client_keys.append t.cctxt src_sk bytes
    (* ~watermark:(Prevote chain_id) *)

(** Endorsing rights are used to gauge whether a client is authorised to prevote or
    precommit for this block.
 *)

let fetch_endorsing_rights t delegate =
  get_endorsing_rights t delegate >>=? function
  | [{ slots }] ->
     return_some slots
  | _ ->
     return_none

(** Injects a serialized and signed operation into the node *)

(* let get_current_round t =
 *   State.get t.cctxt *)

(* let inject_prevote t ?async hash pkh src_sk =
 *   forge_prevote t hash round block_hash >>=? fun bytes ->
 *   State.record t.cctxt pkh t.level >>=? fun () ->
 *   get_chain_id t >>=? fun chain_id ->
 *   sign_prevote t chain_id src_sk bytes >>=? fun signed_bytes ->
 *   inject_operation t ?async signed_bytes >>=? fun op_hash ->
 *   return op_hash
 * 
 * let check_prevote t pkh current_level =
 *   State.get t.cctxt pkh >>=? function
 *   | Some previous_level ->
 *      if Raw_level.(level = previous_level) then
 *        Error_monad.failwith "Duplicate prevote at height = %a" Raw_level.pp previous_level
 *      else
 *        Raw_level.(previous_level >= current_level)
 *   | None ->
 *      return_false
 * 
 * (\* Verbose conditional messages *\)
 * 
 * let context_prevote_error t =
 *   "A prevote at a greater height %a has already occured." Raw_level.pp t.level
 * 
 * let context_prevote_message t name op_hash =
 *   "Injected prevote at height %a, contract %s '%a'"
 *     Raw_level.pp t.level
 *     name
 *     Operation_hash.pp_short op_hash
 *   
 * (\* Create a prevote *\)
 * 
 * let create_prevote t ?async src_sk src_pk =
 *   let src_pkh = public_key_hash src_pk in
 *   get_block_metadata t >>=? fun { protocol_data = { level = { level } } } ->
 *   check_prevote t src_pkh level >>=? function
 *   | true ->
 *      t.cctxt#error (context_prevote_error t)
 *   | false ->
 *      hash_block t >>=? fun hash ->
 *      inject_prevote t ?async src_sk src_pkh >>=? fun op_hash ->
 *      get_client_key t src_pkh >>=? fun (name, _pk, _sk) ->
 *      t.cctxt#message (context_prevote_message t name op_hash) >>= fun () ->
 *      return op_hash *)
     
