
type t = int32

type raw_height = t

include (Compare.Int32 : Compare.S with type t := t)

let encoding = Data_encoding.int32

let pp ppf height = Format.fprintf ppf "%ld" height

let rpc_arg =
  let construct raw_height = Int32.to_string raw_height in
  let destruct str =
    match Int32.of_string str with
    | exception _ ->
       Error "Failed to parse height"
    | raw_height ->
       Ok raw_height
  in
  RPC_arg.make
    ~descr:"An integer representing the current consensus instance"
    ~name: "block_height"
    ~construct
    ~destruct
    ()

let root = 0l
let succ = Int32.succ
let pred h =
  if h = 0l then
    None
  else
    Some (Int32.pred h)

let diff = Int32.sub

let to_int32 h = h
let of_int32_exn h =
  if Compare.Int32.(h >= 0l) then
    h
  else
    invalid_arg "Height_repr.of_int32"

type error += Unexpected_height of Int32.t (* `Permanent *)

let () =
  register_error_kind
    `Permanent
    ~id:"unexpected_height"
    ~title:"Unexpected height"
    ~description:"The block height must be non-negative."
    ~pp:(fun ppf h ->
        Format.fprintf ppf "The height is %s but should be non-negative." (Int32.to_string h))
    Data_encoding.(obj1 (req "height" int32))
    (function Unexpected_height h -> Some h | _ -> None)
    (fun h -> Unexpected_height h)

let of_int32 h =
  try Ok (of_int32_exn h)
  with _ -> Error [Unexpected_height h]

module Index = struct
  type t = raw_height
  let path_length = 1
  let to_path height h = Int32.to_string height :: h
  let of_path = function
    | [s] -> begin
        try Some (Int32.of_string s)
        with _ -> None
      end
    | _ -> None
  let rpc_arg = rpc_arg
  let encoding = encoding
  let compare = compare
end
