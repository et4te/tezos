(** The shell's notion of a height: an integer indicating the number of blocks
    since genesis: genesis is 0, all other blocks have increasing heights from
    there. *)
type t
type raw_height = t
val encoding: raw_height Data_encoding.t
val rpc_arg: raw_height RPC_arg.arg
val pp: Format.formatter -> raw_height -> unit
include Compare.S with type t := raw_height

val to_int32: raw_height -> int32
val of_int32_exn: int32 -> raw_height
val of_int32: int32 -> raw_height tzresult

val diff: raw_height -> raw_height -> int32

val root: raw_height

val succ: raw_height -> raw_height
val pred: raw_height -> raw_height option

module Index : Storage_description.INDEX with type t = raw_height
