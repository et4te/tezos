type t = int32

type raw_round = t

include (Compare.Int32 : Compare.S with type t := t)

let encoding = Data_encoding.int32

let pp ppf round = Format.fprintf ppf "%ld" round

let rpc_arg =
  let construct raw_round = Int32.to_string raw_round in
  let destruct str =
    match Int32.of_string str with
    | exception _ ->
       Error "Failed to parse round"
    | raw_round ->
       Ok raw_round
  in
  RPC_arg.make
    ~descr:"An integer representing the start of a new consensus round"
    ~name: "round"
    ~construct
    ~destruct
    ()

let root = 0l
let succ = Int32.succ
let pred r =
  if r = 0l then
    None
  else
    Some (Int32.pred r)

let diff = Int32.sub

let to_int32 r = r
let of_int32_exn r =
  if Compare.Int32.(r >= 0l) then
    r
  else
    invalid_arg "Round_repr.of_int32"

type error += Unexpected_round of Int32.t (* `Permanent *)

let () =
  register_error_kind
    `Permanent
    ~id:"unexpected_round"
    ~title:"Unexpected round"
    ~description:"The block round must be non-negative."
    ~pp:(fun ppf r ->
        Format.fprintf ppf "The round is %s but should be non-negative." (Int32.to_string r))
    Data_encoding.(obj1 (req "round" int32))
    (function Unexpected_round r -> Some r | _ -> None)
    (fun r -> Unexpected_round r)

let of_int32 r =
  try Ok (of_int32_exn r)
  with _ -> Error [Unexpected_round r]

module Index = struct
  type t = raw_round
  let path_length = 1
  let to_path round r = Int32.to_string round :: r
  let of_path = function
    | [s] -> begin
        try Some (Int32.of_string s)
        with _ -> None
      end
    | _ -> None
  let rpc_arg = rpc_arg
  let encoding = encoding
  let compare = compare
end
